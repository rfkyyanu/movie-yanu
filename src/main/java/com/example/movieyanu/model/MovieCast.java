package com.example.movieyanu.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "movie_cast")
public class MovieCast {

    @EmbeddedId
    private MovieCastKey id;

    @ManyToOne
    @MapsId("mov_id")
    @JoinColumn(name = "mov_id")
    private Movie movie;

    @ManyToOne
    @MapsId("act_id")
    @JoinColumn(name = "act_id")
    private Actor actor;

    @Column
    private String role;

    public MovieCast() {
    }

    public MovieCast(Movie movie, Actor actor, String role) {
        this.movie = movie;
        this.actor = actor;
        this.role = role;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}