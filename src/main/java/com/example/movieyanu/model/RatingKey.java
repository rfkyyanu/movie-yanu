package com.example.movieyanu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RatingKey implements Serializable {

    @Column(name = "mov_id")
    private Long movieId;

    @Column(name = "rev_id")
    private Long reviewerId;

    public RatingKey() {
    }

    public RatingKey(Long movieId, Long reviewerId) {
        this.movieId = movieId;
        this.reviewerId = reviewerId;
    }

    public Long getMovieId() {
        return this.movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Long getReviewerId() {
        return this.reviewerId;
    }

    public void setReviewerId(Long reviewerId) {
        this.reviewerId = reviewerId;
    }

}