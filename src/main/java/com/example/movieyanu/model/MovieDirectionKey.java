package com.example.movieyanu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieDirectionKey implements Serializable {

    @Column(name = "mov_id")
    private Long movieId;

    @Column(name = "dir_id")
    private Long directorId;

    public MovieDirectionKey() {
    }

    public MovieDirectionKey(Long movieId, Long directorId) {
        this.movieId = movieId;
        this.directorId = directorId;
    }

    public Long getMovieId() {
        return this.movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Long getDirectorId() {
        return this.directorId;
    }

    public void setDirectorId(Long directorId) {
        this.directorId = directorId;
    }

}