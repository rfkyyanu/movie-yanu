package com.example.movieyanu.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "movie")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mov_id")
    private Long id;

    @Column(name = "mov_title", nullable = false)
    private String title;

    @Column(name = "mov_year", nullable = false)
    private Integer year;
    
    @Column(name = "mov_time")
    private Integer time;
    
    @Column(name = "mov_lang")
    private String language;
    
    @Column(name = "mov_dt_rel")
    private Date releaseDate;
    
    @Column(name = "mov_rel_country")
    private String releaseCountry;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
    private Set<MovieGenres> movieGenres;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
    private Set<Rating> ratings;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
    private Set<MovieCast> movieCasts;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
    private Set<MovieDirection> movieDirection;

    public Movie() {}

    public Movie(Long id, String title, Integer year, Integer time, String language, Date releaseDate, String releaseCountry) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.time = time;
        this.language = language;
        this.releaseDate = releaseDate;
        this.releaseCountry = releaseCountry;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    public String getReleaseCountry() {
        return releaseCountry;
    }

    public void setReleaseCountry(String releaseCountry) {
        this.releaseCountry = releaseCountry;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

}