package com.example.movieyanu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieGenresKey implements Serializable {

    @Column(name = "mov_id")
    private Long movieId;

    @Column(name = "gen_id")
    private Long genresId;

    public MovieGenresKey() {
    }

    public MovieGenresKey(Long movieId, Long genresId) {
        this.movieId = movieId;
        this.genresId = genresId;
    }

    public Long getMovieId() {
        return this.movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Long getGenresId() {
        return this.genresId;
    }

    public void setGenresId(Long genresId) {
        this.genresId = genresId;
    }

}