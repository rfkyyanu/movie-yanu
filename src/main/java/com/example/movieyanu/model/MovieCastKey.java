package com.example.movieyanu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieCastKey implements Serializable {

    @Column(name = "mov_id")
    private Long movieId;

    @Column(name = "act_id")
    private Long actorId;

    public MovieCastKey(Long movieId, Long actorId) {
        this.movieId = movieId;
        this.actorId = actorId;
    }

    public Long getMovieId() {
        return this.movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Long getActorId() {
        return this.actorId;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

}