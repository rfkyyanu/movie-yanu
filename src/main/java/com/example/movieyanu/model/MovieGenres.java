package com.example.movieyanu.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "movie_genres")
public class MovieGenres {

    @EmbeddedId
    private MovieGenresKey id;
    
    @ManyToOne
    @MapsId("mov_id")
    @JoinColumn(name = "mov_id")
    private Movie movie;

    @ManyToOne
    @MapsId("gen_id")
    @JoinColumn(name = "gen_id")
    private Genres genres;

    public MovieGenres() {
    }

    public MovieGenres(MovieGenresKey id, Movie movie, Genres genres) {
        this.id = id;
        this.movie = movie;
        this.genres = genres;
    }

    public MovieGenresKey getId() {
        return this.id;
    }

    public void setId(MovieGenresKey id) {
        this.id = id;
    }

    public Movie getMovie() {
        return this.movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Genres getGenres() {
        return this.genres;
    }

    public void setGenres(Genres genres) {
        this.genres = genres;
    }

}