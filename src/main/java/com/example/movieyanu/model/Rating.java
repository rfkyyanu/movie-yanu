package com.example.movieyanu.model;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "rating")
public class Rating {

    @EmbeddedId
    private RatingKey id;

    @ManyToOne
    @MapsId("mov_id")
    @JoinColumn(name = "mov_id")
    private Movie movie;

    @ManyToOne
    @MapsId("rev_id")
    @JoinColumn(name = "rev_id")
    private Reviewer reviewer;

    @Column(name = "rev_stars")
    private Integer stars;

    @Column(name = "num_o_ratings")
    private Integer numRatings;

    public Rating() {
    }

    public Rating(RatingKey id, Movie movie, Reviewer reviewer, Integer stars, Integer numRatings) {
        this.id = id;
        this.movie = movie;
        this.reviewer = reviewer;
        this.stars = stars;
        this.numRatings = numRatings;
    }

    public RatingKey getId() {
        return this.id;
    }

    public void setId(RatingKey id) {
        this.id = id;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Integer getNumRatings() {
        return numRatings;
    }
    
    public void setNumRatings(Integer numRatings) {
        this.numRatings = numRatings;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public Reviewer getReviewer() {
        return reviewer;
    }

    public void setReviewer(Reviewer reviewer) {
        this.reviewer = reviewer;
    }

}