package com.example.movieyanu.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import com.example.movieyanu.dto.GenresDTO;
import com.example.movieyanu.model.Genres;
import com.example.movieyanu.repository.GenresRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/genres")
public class GenresController {

    @Autowired
    GenresRepository genresRepository;

    // Get all genres
    @GetMapping("/show/all")
    public HashMap<String, Object> getAllGenres() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<GenresDTO> genresList = new ArrayList<GenresDTO>();

        for (Genres tempGen : genresRepository.findAll()) {
            GenresDTO genresDTO = new GenresDTO(tempGen.getId(), tempGen.getTitle());
            genresList.add(genresDTO);
        }

        result.put("Message", "Read Success");
        result.put("Total", genresList.size());
        result.put("Data", genresList);

        return result;
    }

    // Create genres
    @PostMapping("/create")
    public HashMap<String, Object> createGenres(@Valid @RequestBody GenresDTO genresDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Genres newGenres = new Genres(genresDTO.getId(), genresDTO.getTitle());

        result.put("Message", "Create Success");
        result.put("Data", genresRepository.save(newGenres));

        return result;
    }

    // Update genres
    @PutMapping("/update")
    public HashMap<String, Object> updateGenres(@RequestParam(name = "id") Long genresId, @RequestBody GenresDTO genresDTODetails) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Genres updatedGenres = genresRepository.findById(genresId).get();
        
        if (genresDTODetails.getTitle() != null) {
            updatedGenres.setTitle(genresDTODetails.getTitle());
        } else {
            updatedGenres.setTitle(updatedGenres.getTitle());
        }
        
        result.put("Message", "Update Success");
        result.put("Data", genresRepository.save(updatedGenres));

        return result;
    }

    // Delete genres
    @DeleteMapping("/delete")
    public HashMap<String, Object> deleteGenres(@RequestParam(name = "id") Long genresId) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        genresRepository.delete(genresRepository.findById(genresId).get());

        result.put("Message", "Delete Success");

        return result;
    }

}