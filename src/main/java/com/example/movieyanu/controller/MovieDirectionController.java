package com.example.movieyanu.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import com.example.movieyanu.dto.MovieDirectionDTO;
import com.example.movieyanu.model.MovieDirection;
import com.example.movieyanu.repository.MovieDirectionRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/movie-direction")
public class MovieDirectionController {

    @Autowired
    MovieDirectionRepository movieDirectionRepository;

    ModelMapper modelMapper = new ModelMapper();

    // Get all Movie Direction
    @GetMapping("/show/all")
    public HashMap<String, Object> getAllMovieDirection() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<MovieDirectionDTO> movieDirectionList = new ArrayList<MovieDirectionDTO>();

        for (MovieDirection tempMov : movieDirectionRepository.findAll()) {
            movieDirectionList.add(convertToDTO(tempMov));
        }

        result.put("Message", "Success");
        result.put("Total", movieDirectionList.size());
        result.put("Data", movieDirectionList);

        return result;
    }

    // Create new movie Direction
    @PostMapping("/create")
    public HashMap<String, Object> createMovieDirection(@Valid @RequestParam MovieDirectionDTO movieDirectionDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<MovieDirectionDTO> movieDirectionList = new ArrayList<MovieDirectionDTO>();

        movieDirectionRepository.save(convertToEntity(movieDirectionDTO));

        result.put("Message", "Success");
        result.put("Total", movieDirectionList.size());
        result.put("Data", movieDirectionList);

        return result;
    }

    // Update movie Direction
    @PutMapping("/update")
    public HashMap<String, Object> updateMovieDirection(@RequestParam(name = "id") Long movieDirectionId, @Valid @RequestBody MovieDirectionDTO movieDirectionDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        
        MovieDirection updatedMovieDirection = movieDirectionRepository.findById(movieDirectionId).get();

        updatedMovieDirection.setDirector(convertToEntity(movieDirectionDTO).getDirector());
        updatedMovieDirection.setMovie(convertToEntity(movieDirectionDTO).getMovie());

        result.put("Message", "Success");
        result.put("Data", updatedMovieDirection);

        return result;
    }

    // Delete movie Direction
    @DeleteMapping("/delete")
    public HashMap<String, Object> deleteMovieDirection(@RequestParam(name = "id") Long movieDirectionId) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        movieDirectionRepository.delete(movieDirectionRepository.findById(movieDirectionId).get());

        result.put("Message", "Delete Success");

        return result;
    }

    public MovieDirectionDTO convertToDTO(MovieDirection movieDirection) {
        return modelMapper.map(movieDirection, MovieDirectionDTO.class);
    }

    public MovieDirection convertToEntity(MovieDirectionDTO movieDirectionDTO) {
        return modelMapper.map(movieDirectionDTO, MovieDirection.class);
    }

}