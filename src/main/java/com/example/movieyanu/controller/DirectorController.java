package com.example.movieyanu.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import com.example.movieyanu.dto.DirectorDTO;
import com.example.movieyanu.model.Director;
import com.example.movieyanu.repository.DirectorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/director")
public class DirectorController {

    @Autowired
    DirectorRepository directorRepository;

    // Get all Director
    @GetMapping("/show/all")
    public HashMap<String, Object> getAllDirector() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<DirectorDTO> directorList = new ArrayList<DirectorDTO>();

        for (Director tempDir : directorRepository.findAll()) {
            DirectorDTO directorDTO = new DirectorDTO(tempDir.getId(),
                                                      tempDir.getFirstName(),
                                                      tempDir.getLastName());
            directorList.add(directorDTO);
        }

        result.put("Message", "Read Success");
        result.put("Total", directorList.size());
        result.put("Data", directorList);

        return result;
    }

    // Create director
    @PostMapping("/create")
    public HashMap<String, Object> createDirector(@Valid @RequestBody DirectorDTO directorDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Director newDirector = new Director(directorDTO.getId(), 
                                            directorDTO.getFirstName(), 
                                            directorDTO.getLastName());

        result.put("Message", "Create Success");
        result.put("Data", directorRepository.save(newDirector));

        return result;
    }

    // Update Director
    @PutMapping("/update")
    public HashMap<String, Object> updateDirector(@RequestParam(name = "id") Long directorId, @RequestBody DirectorDTO directorDTODetails) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Director updatedDirector = directorRepository.findById(directorId).get();
        
        if (directorDTODetails.getFirstName() != null) {
            updatedDirector.setFirstName(directorDTODetails.getFirstName());
        } else {
            updatedDirector.setFirstName(updatedDirector.getFirstName());
        }

        if (directorDTODetails.getLastName() != null) {
            updatedDirector.setLastName(directorDTODetails.getLastName());
        } else {
            updatedDirector.setLastName(updatedDirector.getLastName());
        }
        
        result.put("Message", "Update Success");
        result.put("Data", directorRepository.save(updatedDirector));

        return result;
    }
    
    // Delete director
    @DeleteMapping("/delete")
    public HashMap<String, Object> deleteDirector(@RequestParam(name = "id") Long directorId) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        directorRepository.delete(directorRepository.findById(directorId).get());

        result.put("Message", "Delete Success");

        return result;
    }

}