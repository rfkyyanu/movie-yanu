package com.example.movieyanu.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import com.example.movieyanu.dto.MovieCastDTO;
import com.example.movieyanu.model.MovieCast;
import com.example.movieyanu.repository.MovieCastRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/movie-cast")
public class MovieCastController {

    @Autowired
    MovieCastRepository movieCastRepository;

    ModelMapper modelMapper = new ModelMapper();

    // Get all Movie Cast
    @GetMapping("/show/all")
    public HashMap<String, Object> getAllMovieCast() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<MovieCastDTO> movieCastList = new ArrayList<MovieCastDTO>();

        for (MovieCast tempMov : movieCastRepository.findAll()) {
            movieCastList.add(convertToDTO(tempMov));
        }

        result.put("Message", "Success");
        result.put("Total", movieCastList.size());
        result.put("Data", movieCastList);

        return result;
    }

    // Create new movie cast
    @PostMapping("/create")
    public HashMap<String, Object> createMovieCast(@Valid @RequestBody MovieCastDTO movieCastDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<MovieCastDTO> movieCastList = new ArrayList<MovieCastDTO>();

        movieCastRepository.save(convertToEntity(movieCastDTO));

        result.put("Message", "Success");
        result.put("Total", movieCastList.size());
        result.put("Data", movieCastList);

        return result;
    }

    // Update movie cast
    @PutMapping("/update")
    public HashMap<String, Object> updateMovieCast(@RequestParam(name = "id") Long movieCastId, @Valid @RequestBody MovieCastDTO movieCastDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        
        MovieCast updatedMovieCast = movieCastRepository.findById(movieCastId).get();

        updatedMovieCast.setActor(convertToEntity(movieCastDTO).getActor());
        updatedMovieCast.setMovie(convertToEntity(movieCastDTO).getMovie());

        result.put("Message", "Success");
        result.put("Data", updatedMovieCast);

        return result;
    }

    // Delete movie cast
    @DeleteMapping("/delete")
    public HashMap<String, Object> deleteMovieCast(@RequestParam(name = "id") Long movieCastId) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        movieCastRepository.delete(movieCastRepository.findById(movieCastId).get());

        result.put("Message", "Delete Success");

        return result;
    }

    public MovieCastDTO convertToDTO(MovieCast movieCast) {
        return modelMapper.map(movieCast, MovieCastDTO.class);
    }

    public MovieCast convertToEntity(MovieCastDTO movieCastDTO) {
        return modelMapper.map(movieCastDTO, MovieCast.class);
    }
    
}