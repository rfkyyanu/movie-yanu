package com.example.movieyanu.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import com.example.movieyanu.dto.MovieGenresDTO;
import com.example.movieyanu.model.MovieGenres;
import com.example.movieyanu.repository.MovieGenresRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/movie-genres")
public class MovieGenresController {

    @Autowired
    MovieGenresRepository movieGenresRepository;

    ModelMapper modelMapper = new ModelMapper();

    // Get all Movie Genres
    @GetMapping("/show/all")
    public HashMap<String, Object> getAllMovieGenres() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<MovieGenresDTO> movieGenresList = new ArrayList<MovieGenresDTO>();

        for (MovieGenres tempMov : movieGenresRepository.findAll()) {
            movieGenresList.add(convertToDTO(tempMov));
        }

        result.put("Message", "Success");
        result.put("Total", movieGenresList.size());
        result.put("Data", movieGenresList);

        return result;
    }

    // Create new movie Genres
    @PostMapping("/create")
    public HashMap<String, Object> createMovieGenres(@Valid @RequestParam MovieGenresDTO movieGenresDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<MovieGenresDTO> movieGenresList = new ArrayList<MovieGenresDTO>();

        movieGenresRepository.save(convertToEntity(movieGenresDTO));

        result.put("Message", "Success");
        result.put("Total", movieGenresList.size());
        result.put("Data", movieGenresList);

        return result;
    }

    // Update movie Genres
    @PutMapping("/update")
    public HashMap<String, Object> updateMovieGenres(@RequestParam(name = "id") Long movieGenresId, @Valid @RequestBody MovieGenresDTO movieGenresDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        
        MovieGenres updatedMovieGenres = movieGenresRepository.findById(movieGenresId).get();

        updatedMovieGenres.setGenres(convertToEntity(movieGenresDTO).getGenres());
        updatedMovieGenres.setMovie(convertToEntity(movieGenresDTO).getMovie());

        result.put("Message", "Success");
        result.put("Data", updatedMovieGenres);

        return result;
    }

    // Delete movie Genres
    @DeleteMapping("/delete")
    public HashMap<String, Object> deleteMovieGenres(@RequestParam(name = "id") Long movieGenresId) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        movieGenresRepository.delete(movieGenresRepository.findById(movieGenresId).get());

        result.put("Message", "Delete Success");

        return result;
    }

    public MovieGenresDTO convertToDTO(MovieGenres movieGenres) {
        return modelMapper.map(movieGenres, MovieGenresDTO.class);
    }

    public MovieGenres convertToEntity(MovieGenresDTO movieGenresDTO) {
        return modelMapper.map(movieGenresDTO, MovieGenres.class);
    }

}