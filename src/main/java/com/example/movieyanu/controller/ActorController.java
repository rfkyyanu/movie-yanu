package com.example.movieyanu.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import com.example.movieyanu.dto.ActorDTO;
import com.example.movieyanu.model.Actor;
import com.example.movieyanu.repository.ActorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("api/actor")
public class ActorController {

    @Autowired
    ActorRepository actorRepository;

    // Get all actor
    @GetMapping("/show/all")
    public HashMap<String, Object> getAllActor() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<ActorDTO> actorList = new ArrayList<ActorDTO>();

        for (Actor tempAct : actorRepository.findAll()) {
            ActorDTO actorDTO = new ActorDTO(tempAct.getId(),
                                             tempAct.getFirstName(),
                                             tempAct.getLastName(), 
                                             tempAct.getGender());
            actorList.add(actorDTO);
        }

        result.put("Message", "Read Success");
        result.put("Total", actorList.size());
        result.put("Data", actorList);

        return result;
    }

    // Create new Actor
    @PostMapping("/create")
    public HashMap<String, Object> createActor(@Valid @RequestBody ActorDTO actorDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Actor newActor = new Actor(actorDTO.getId(), 
                                   actorDTO.getFirstName(), 
                                   actorDTO.getLastName(), 
                                   actorDTO.getGender());

        result.put("Message", "Create Success");
        result.put("Data", actorRepository.save(newActor));

        return result;
    }
    
    // Update actor
    @PutMapping("/update")
    public HashMap<String, Object> updateActor(@RequestParam(name = "id") Long actorId, @RequestBody ActorDTO actorDTODetails) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Actor updatedActor = actorRepository.findById(actorId).get();
        
        if (actorDTODetails.getFirstName() != null) {
            updatedActor.setFirstName(actorDTODetails.getFirstName());
        } else {
            updatedActor.setFirstName(updatedActor.getFirstName());
        }

        if (actorDTODetails.getLastName() != null) {
            updatedActor.setLastName(actorDTODetails.getLastName());
        } else {
            updatedActor.setLastName(updatedActor.getLastName());
        }

        if (actorDTODetails.getGender() != null) {
            updatedActor.setGender(actorDTODetails.getGender());
        } else {
            updatedActor.setGender(updatedActor.getGender());
        }
        
        result.put("Message", "Update Success");
        result.put("Data", actorRepository.save(updatedActor));

        return result;
    }

    // Delete actor
    @DeleteMapping("/delete")
    public HashMap<String, Object> deleteActor(@RequestParam(name = "id") Long actorId) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        actorRepository.delete(actorRepository.findById(actorId).get());

        result.put("Message", "Delete Success");

        return result;
    }

}