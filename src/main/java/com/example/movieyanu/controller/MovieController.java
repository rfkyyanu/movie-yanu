package com.example.movieyanu.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import com.example.movieyanu.dto.MovieDTO;
import com.example.movieyanu.model.Movie;
import com.example.movieyanu.repository.MovieRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/movie")
public class MovieController {

    @Autowired
    MovieRepository movieRepository;

    // Get all movie
    @GetMapping("/show/all")
    public HashMap<String, Object> getAllMovies() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<MovieDTO> movieList = new ArrayList<MovieDTO>();

        for (Movie tempMov : movieRepository.findAll()) {
            MovieDTO movieDTO = new MovieDTO(tempMov.getId(), 
                                             tempMov.getTitle(), 
                                             tempMov.getYear(), 
                                             tempMov.getTime(), 
                                             tempMov.getLanguage(), 
                                             tempMov.getReleaseDate(), 
                                             tempMov.getReleaseCountry());
            movieList.add(movieDTO);
        }

        result.put("Message", "Read Success");
        result.put("Total", movieList.size());
        result.put("Data", movieList);

        return result;
    }

    // Create new movie
    @PostMapping("/create")
    public HashMap<String, Object> createMovie(@Valid @RequestBody MovieDTO movieDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Movie newMovie = new Movie(movieDTO.getId(), 
                                   movieDTO.getTitle(), 
                                   movieDTO.getYear(), 
                                   movieDTO.getTime(), 
                                   movieDTO.getLanguage(), 
                                   movieDTO.getReleaseDate(), 
                                   movieDTO.getReleaseCountry());

        result.put("Message", "Create Success");
        result.put("Data", movieRepository.save(newMovie));

        return result;
    }

    // Update movie
    @PutMapping("/update")
    public HashMap<String, Object> updateMovie(@RequestParam(name = "id") Long movieId, @RequestBody MovieDTO movieDTODetails) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Movie updatedMovie = movieRepository.findById(movieId).get();

        if (movieDTODetails.getTitle() != null) {
            updatedMovie.setTitle(movieDTODetails.getTitle());
        } else {
            updatedMovie.setTitle(updatedMovie.getTitle());
        }

        if (movieDTODetails.getYear() != null) {
            updatedMovie.setYear(movieDTODetails.getYear());
        } else {
            updatedMovie.setYear(updatedMovie.getYear());
        }

        if (movieDTODetails.getTime() != null) {
            updatedMovie.setTime(movieDTODetails.getTime());
        } else {
            updatedMovie.setTime(updatedMovie.getTime());
        }

        if (movieDTODetails.getLanguage() != null) {
            updatedMovie.setLanguage(movieDTODetails.getLanguage());
        } else {
            updatedMovie.setLanguage(updatedMovie.getLanguage());
        }

        if (movieDTODetails.getReleaseDate() != null) {
            updatedMovie.setReleaseDate(movieDTODetails.getReleaseDate());
        } else {
            updatedMovie.setReleaseDate(updatedMovie.getReleaseDate());
        }

        if (movieDTODetails.getReleaseCountry() != null) {
            updatedMovie.setReleaseCountry(movieDTODetails.getReleaseCountry());
        } else {
            updatedMovie.setReleaseCountry(updatedMovie.getReleaseCountry());
        }

        result.put("Message", "Update Success");
        result.put("Data", movieRepository.save(updatedMovie));

        return result;
    }

    // Delete movie
    @DeleteMapping("/delete")
    public HashMap<String, Object> deleteMovie(@RequestParam(name = "id") Long movieId) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        movieRepository.delete(movieRepository.findById(movieId).get());

        result.put("Message", "Delete Success");

        return result;
    }

}