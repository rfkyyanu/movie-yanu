package com.example.movieyanu.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import com.example.movieyanu.dto.RatingDTO;
import com.example.movieyanu.model.Rating;
import com.example.movieyanu.repository.RatingRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rating")
public class RatingController {

    @Autowired
    RatingRepository ratingRepository;

    ModelMapper modelMapper = new ModelMapper();

    // Get all Movie Rating
    @GetMapping("/show/all")
    public HashMap<String, Object> getAllMovieRating() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<RatingDTO> ratingList = new ArrayList<RatingDTO>();

        for (Rating tempRat : ratingRepository.findAll()) {
            ratingList.add(convertToDTO(tempRat));
        }

        result.put("Message", "Success");
        result.put("Total", ratingList.size());
        result.put("Data", ratingList);

        return result;
    }

    // Create new movie Rating
    @PostMapping("/create")
    public HashMap<String, Object> createMovieRating(@Valid @RequestParam RatingDTO ratingDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<RatingDTO> ratingList = new ArrayList<RatingDTO>();

        ratingRepository.save(convertToEntity(ratingDTO));

        result.put("Message", "Success");
        result.put("Total", ratingList.size());
        result.put("Data", ratingList);

        return result;
    }

    // Update movie Rating
    @PutMapping("/update")
    public HashMap<String, Object> updateMovieRating(@RequestParam(name = "id") Long ratingId, @Valid @RequestBody RatingDTO ratingDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        
        Rating updatedRating = ratingRepository.findById(ratingId).get();

        updatedRating.setReviewer(convertToEntity(ratingDTO).getReviewer());
        updatedRating.setMovie(convertToEntity(ratingDTO).getMovie());

        result.put("Message", "Success");
        result.put("Data", updatedRating);

        return result;
    }

    // Delete movie Rating
    @DeleteMapping("/delete")
    public HashMap<String, Object> deleteMovieRating(@RequestParam(name = "id") Long ratingId) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ratingRepository.delete(ratingRepository.findById(ratingId).get());

        result.put("Message", "Delete Success");

        return result;
    }

    public RatingDTO convertToDTO(Rating rating) {
        return modelMapper.map(rating, RatingDTO.class);
    }

    public Rating convertToEntity(RatingDTO ratingDTO) {
        return modelMapper.map(ratingDTO, Rating.class);
    }

}