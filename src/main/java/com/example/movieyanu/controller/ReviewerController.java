package com.example.movieyanu.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import com.example.movieyanu.dto.ReviewerDTO;
import com.example.movieyanu.model.Reviewer;
import com.example.movieyanu.repository.ReviewerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/reviewer")
public class ReviewerController {

    @Autowired
    ReviewerRepository reviewerRepository;

    // Get all reviewer
    @GetMapping("/show/all")
    public HashMap<String, Object> getAllReviewer() {
        HashMap<String, Object> result = new HashMap<String, Object>();
        ArrayList<ReviewerDTO> reviewerList = new ArrayList<ReviewerDTO>();

        for (Reviewer tempRev : reviewerRepository.findAll()) {
            ReviewerDTO genresDTO = new ReviewerDTO(tempRev.getId(), tempRev.getName());
            reviewerList.add(genresDTO);
        }

        result.put("Message", "Read Success");
        result.put("Total", reviewerList.size());
        result.put("Data", reviewerList);

        return result;
    }

    // Create reviewer
    @PostMapping("/create")
    public HashMap<String, Object> createReviewer(@Valid @RequestBody ReviewerDTO reviewerDTO) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Reviewer newReviewer = new Reviewer(reviewerDTO.getId(), reviewerDTO.getName());

        result.put("Message", "Create Success");
        result.put("Data", reviewerRepository.save(newReviewer));

        return result;
    }

    // Update reviewer
    @PutMapping("/update")
    public HashMap<String, Object> updateReviewer(@RequestParam(name = "id") Long reviewerId, @RequestBody ReviewerDTO reviewerDTODetails) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        Reviewer updatedReviewer = reviewerRepository.findById(reviewerId).get();
        
        if (reviewerDTODetails.getName() != null) {
            updatedReviewer.setName(reviewerDTODetails.getName());
        } else {
            updatedReviewer.setName(updatedReviewer.getName());
        }
        
        result.put("Message", "Update Success");
        result.put("Data", reviewerRepository.save(updatedReviewer));

        return result;
    }

    // Delete reviewer
    @DeleteMapping("/delete")
    public HashMap<String, Object> deleteReviewer(@RequestParam(name = "id") Long reviewerId) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        reviewerRepository.delete(reviewerRepository.findById(reviewerId).get());

        result.put("Message", "Delete Success");

        return result;
    }

}