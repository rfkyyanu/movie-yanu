package com.example.movieyanu.dto;

import java.io.Serializable;

import com.example.movieyanu.model.MovieDirectionKey;

public class MovieDirectionDTO implements Serializable {

    private MovieDirectionKey id;
    private MovieDTO movie;
    private DirectorDTO director;

    public MovieDirectionDTO() {
    }

    public MovieDirectionDTO(MovieDirectionKey id, MovieDTO movie, DirectorDTO director) {
        this.id = id;
        this.movie = movie;
        this.director = director;
    }

    public MovieDirectionKey getId() {
        return this.id;
    }

    public void setId(MovieDirectionKey id) {
        this.id = id;
    }    

    public DirectorDTO getDirector() {
        return director;
    }

    public void setDirector(DirectorDTO director) {
        this.director = director;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO movie) {
        this.movie = movie;
    }

}