package com.example.movieyanu.dto;

import com.example.movieyanu.model.MovieCastKey;

public class MovieCastDTO {

    private MovieCastKey id;
    private MovieDTO movie;
    private ActorDTO actor;
    private String role;

    public MovieCastDTO() {
    }

    public MovieCastDTO(MovieCastKey id, MovieDTO movie, ActorDTO actor, String role) {
        this.id = id;
        this.movie = movie;
        this.actor = actor;
        this.role = role;
    }

    public MovieCastKey getId() {
        return this.id;
    }

    public void setId(MovieCastKey id) {
        this.id = id;
    }

    public ActorDTO getActor() {
        return actor;
    }

    public void setActor(ActorDTO actor) {
        this.actor = actor;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO movie) {
        this.movie = movie;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}