package com.example.movieyanu.dto;

import java.io.Serializable;

public class MovieGenresDTO implements Serializable {

    private MovieDTO movie;
    private GenresDTO genres;

    public MovieGenresDTO() {
    }

    public MovieGenresDTO(MovieDTO movie, GenresDTO genres) {
        this.movie = movie;
        this.genres = genres;
    }

    public GenresDTO getGenres() {
        return genres;
    }

    public void setGenres(GenresDTO genres) {
        this.genres = genres;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO movie) {
        this.movie = movie;
    }

}