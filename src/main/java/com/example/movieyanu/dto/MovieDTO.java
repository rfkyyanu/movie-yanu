package com.example.movieyanu.dto;

import java.util.Date;

public class MovieDTO {

    private Long id;
    private String title;
    private Integer year;
    private Integer time;
    private String language;
    private Date releaseDate;
    private String releaseCountry;

    public MovieDTO() {}

    public MovieDTO(Long id, String title, Integer year, Integer time, String language, Date releaseDate, String releaseCountry) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.time = time;
        this.language = language;
        this.releaseDate = releaseDate;
        this.releaseCountry = releaseCountry;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    public String getReleaseCountry() {
        return releaseCountry;
    }

    public void setReleaseCountry(String releaseCountry) {
        this.releaseCountry = releaseCountry;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

}