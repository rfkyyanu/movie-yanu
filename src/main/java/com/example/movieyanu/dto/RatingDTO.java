package com.example.movieyanu.dto;

import java.io.Serializable;

public class RatingDTO implements Serializable {

    private MovieDTO movie;
    private ReviewerDTO reviewer;
    private Integer stars;
    private Integer numRatings;

    public RatingDTO() {
    }

    public RatingDTO(MovieDTO movie, ReviewerDTO reviewer, Integer stars, Integer numRatings) {
        this.movie = movie;
        this.reviewer = reviewer;
        this.stars = stars;
        this.numRatings = numRatings;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO movie) {
        this.movie = movie;
    }

    public Integer getNumRatings() {
        return numRatings;
    }
    
    public void setNumRatings(Integer numRatings) {
        this.numRatings = numRatings;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public ReviewerDTO getReviewer() {
        return reviewer;
    }

    public void setReviewer(ReviewerDTO reviewer) {
        this.reviewer = reviewer;
    }

}