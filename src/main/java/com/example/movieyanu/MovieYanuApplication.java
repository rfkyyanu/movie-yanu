package com.example.movieyanu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieYanuApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieYanuApplication.class, args);
	}

}
