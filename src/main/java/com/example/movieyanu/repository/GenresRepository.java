package com.example.movieyanu.repository;

import com.example.movieyanu.model.Genres;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenresRepository extends JpaRepository<Genres, Long>{

}