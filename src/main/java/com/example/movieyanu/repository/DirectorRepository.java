package com.example.movieyanu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.movieyanu.model.Director;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Long> {

}