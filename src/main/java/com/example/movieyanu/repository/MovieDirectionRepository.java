package com.example.movieyanu.repository;

import com.example.movieyanu.model.MovieDirection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieDirectionRepository extends JpaRepository<MovieDirection, Long> {

}