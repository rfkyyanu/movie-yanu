package com.example.movieyanu.repository;

import com.example.movieyanu.model.MovieGenres;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieGenresRepository extends JpaRepository<MovieGenres, Long> {

}