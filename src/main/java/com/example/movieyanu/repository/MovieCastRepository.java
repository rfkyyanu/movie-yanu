package com.example.movieyanu.repository;

import com.example.movieyanu.model.MovieCast;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieCastRepository extends JpaRepository<MovieCast, Long> {

}